<?php
use Illuminate\Support\Facades\Route;

Route::post('auth/login', 'App\Http\Controllers\api\AuthController@login');
Route::group(['middleware' => ['apiJwt']], function(){
    Route::post('auth/logout', 'App\Http\Controllers\api\AuthController@logout');
    Route::apiResource('user', 'App\Http\Controllers\api\UserController');
});
Route::apiResource('dogs', 'App\Http\Controllers\api\DogController');
